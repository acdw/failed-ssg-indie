from flask import current_app
import frontmatter
import mistune
import os
import re


class Post():
    """
    A post object.  Holds metadata and content object.
    """

    def __init__(self, fname):
        # Post filenames are structured like this: <date>-<slug>.<group>
        self.name = os.path.basename(fname)
        fnmatch = re.match(
            r'(20\d{2}-[01]\d-[0-3]\d)-([A-Za-z0-9-]+).(\w+)', self.name)
        if fnmatch:
            self.date, self.slug, self.group = fnmatch.group(1, 2, 3)

        # Split the file into meta and content
        with open(fname, 'r') as f:
            self.meta, self.markdown = frontmatter.parse(f.read())

        render = mistune.Markdown()
        self.html = render(self.markdown)


class PostList():
    def __init__(self, group):
        self.posts = []
        path = current_app.config['SRC_DIR']
        for p in os.listdir(path):
            if p.endswith(group):
                self.posts.append(Post(os.path.join(path, p)))
