"""
Configurations for tuppence
"""

from os import path

# Flask config vars
PREFERRED_URL_SCHEME = "https"

# Tuppence config vars
SRC_DIR = path.join(path.dirname(path.realpath(__file__)), 'content')
