"""tuppence
an indie-web capable flask thing
"""

from os import path
from flask import (Flask, render_template)
from werkzeug.contrib.fixers import ProxyFix
from .posts import PostList

app = Flask(__name__)
app.config.from_object('tuppence.config')
app.wsgi_app = ProxyFix(app.wsgi_app)


@app.route("/")
def hello():
    return "🚧tuppence is under construction🚧"


@app.route("/<group>/<dateslug>")
def single(group, dateslug):
    """Show an individual post from a group"""
    post_obj = PostList(group)
    posts = post_obj.posts

    for post in posts:
        if path.splitext(post.name)[0] == dateslug:
            return render_template('post.html',
                                   title=post.meta['title'] or "?",
                                   content=post.html)

    return page_not_found(404)


@app.errorhandler(404)
def page_not_found(e):
    return 'oh shit waddup 404', 404
