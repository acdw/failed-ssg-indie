"""
Tuppence test suite.
"""

import pytest
from tuppence import app


@pytest.fixture
def client():
    return app.test_client()


def test_index():
    """Test the index."""
    rv = client().get('/')
    assert b'tuppence' in rv.data
