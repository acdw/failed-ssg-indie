#!/bin/sh

# Because a Makefile won't cut it

export VENV=venv

arg="$1"
if [ -z "$arg" ]; then
    arg=default
fi

case "$arg" in
    run|default)
        cmd="gunicorn --reload tuppence:app"
        str="Running"
        ;;
    test)
        cmd="python3 setup.py test"
        str="Testing"
        ;;
    *)
        echo "Unknown command"
        exit 1
        ;;
esac

log() {
    printf '\033[0;37;46m %s \033[0;m\n' "$1"
}

if [ ! -d $VENV ]; then
    log "Setting up virtualenv..."
    virtualenv -p python3 $VENV
fi

log "Activating..."
. $VENV/bin/activate

log "Installing tuppence..."
pip3 install .
log "Installing gunicorn..."
pip3 install gunicorn

log "${str}...($cmd)"
$cmd
