# Design ideas for tuppence

I want to lay this out to keep focused on my goals and work toward them
steadily.

I want `tuppence` to take markdown files from a source directory on my public
server, render and cache them to HTML, and serve those pages in a variety of
ways and through a variety of views.

The markdown files should be written in Markdown with YAML frontmatter, like
so:

```markdown
---
title: Having fun with flask
tags: 
- flask
- example post
date: 2018-03-25T10:43:02-0500
---

# I'm having a great time over here

This blog post will be rendered into HTML.  Its name on the disk is of the
format <lifeday>-<slug>.group, which means *this* file's name is
10083-having-fun-with-flask.blog.  It can be found at the following URLS (on
the host, obviously):

- example.com/blog/10083/having-fun-with-flask 
  - this specific page
- example.com/blog/10083/having-fun-with-flask/source (maybe?)
  - the source markdown of the generated page
- example.com/10083      
  - show all posts authored on 10083
- example.com/blog       
  - show all posts in the 'blog' group
- example.com/blog/10083 
  - show all posts in 'blog' on 10083
```

The generated views should be cached (using a system like [Flask-FlatPages])
so I don't have to make them over and over again.  And I want to use an
extensible markdown parser like [mistune] so I can add my little extra stuff
to it, like verse formatting.  Other packages to use include
[python-frontmatter] for YAML parsing and ... (TO BE ADDED TO)

[Flask-FlatPages]: https://pythonhosted.org/Flask-FlatPages/
[mistune]: https://github.com/lepture/mistune
[python-frontmatter]: https://github.com/eyeseast/python-frontmatter

I also want to include a micropub endpoint.  It should be pretty simple: I
want to author my posts in Markdown in a textbox and have the endpoint upload
that Markdown to my content folder using the filename syntax given in the
sample Markdown file above.  After that, all the views should automatically be
updated to include the new file (or if not *magically* automatic, after the post
is made I'll call `update()` or whatever to update the cache).
