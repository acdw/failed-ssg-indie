from setuptools import setup
from os import path

here = path.abspath(path.dirname(__file__))
with open(path.join(here, "README.md"), 'r', encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='tuppence',
    version='0.0.1',
    description='An indieweb-enabled flask server thing',
    long_description=long_description,
    url='https://gitlab.com/acdw/tuppence',
    author='Case Duckworth',
    author_email='dev@me.acdw.net',
    packages=['tuppence'],
    include_package_data=True,
    install_requires=[
        'flask',
        'mistune',
        'python-frontmatter',
    ],
    setup_requires=[
        'pytest-runner',
    ],
    tests_require=[
        'pytest',
    ],
)
