# tuppence

*a static site generator for the indieweb*

I was building my site with pandoc, a couple of python scripts, and a
Makefile. That worked okay, but I had to build the site myself at home every
time I made a post, and pandoc 2 is not portable (at least, it's not on my
host yet, so not portable *enough*), and I had no idea how to coordinate the
workflow with an [indieweb-capable] microblog. Combine those frustrations with
the discovery of [susainapai's makesite] and I made up my mind: I need to make
my own CMS for fun and profit (but mostly for learning, I hope). And so
tuppence was born.

  [indieweb-capable]: https://indieweb.org/
  [susainapai's makesite]: https://github.com/sunainapai/makesite
